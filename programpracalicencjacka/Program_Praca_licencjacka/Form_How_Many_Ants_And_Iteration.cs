﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Program_Praca_licencjacka
{
    public partial class Form_How_Many_Ants_And_Iteration : Form
    {
        public Form_How_Many_Ants_And_Iteration()
        {
            InitializeComponent();
        }
        public static string przekaz_liczbe_mrowek;
        public static string przekaz_liczbe_iteracji;

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox_ants.Text == "" || textBox_iterations.Text == "")
            {
                MessageBox.Show("fill in all fields", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                przekaz_liczbe_mrowek = textBox_ants.Text;
                przekaz_liczbe_iteracji = textBox_iterations.Text;
                this.Close();
            }
        }
    }
}
