﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Program_Praca_licencjacka
{
    class Metody
    {
        Random random = new Random(0);

        int alfa = 3;
        int beta = 2;
        double a = 0.01;
        double b = 2.0;
        public int[][] utworz_mrowki(int liczba_mrowek, int liczba_miast)
        {
            int[][] tab_mrowek = new int[liczba_mrowek][];
            for (int i = 0; i < liczba_mrowek; i++)
            {
                int poczatkowe_miasto = 0;
                tab_mrowek[i] = losuj_trase(liczba_miast, poczatkowe_miasto);
            }
            return tab_mrowek;
        }

        public int[] losuj_trase(int liczba_miast, int poczatkowe_miasto)
        {
            int[] tab_trasy = new int[liczba_miast];
            for (int i = 0; i < liczba_miast; i++)
            {
                tab_trasy[i] = i;
            }
            for (int i = 0; i < liczba_miast; i++)
            {
                int wylosowany_index = random.Next(i, liczba_miast);
                int wylosowane_miasto = tab_trasy[wylosowany_index];
                tab_trasy[wylosowany_index] = tab_trasy[i];
                tab_trasy[i] = wylosowane_miasto;
            }
            int index_poczatkowego_miasta = znajdz_poczatkowe_miasto(tab_trasy, poczatkowe_miasto);
            int wartosc_indexu_0 = tab_trasy[0];
            tab_trasy[0] = tab_trasy[index_poczatkowego_miasta];
            tab_trasy[index_poczatkowego_miasta] = wartosc_indexu_0;

            return tab_trasy;
        }

        public int znajdz_poczatkowe_miasto(int[] tab_trasy, int poczatkowe_miasto)
        {
            for (int i = 0; i < tab_trasy.Length; i++)
            {
                if (poczatkowe_miasto == tab_trasy[i])
                {
                    return i;
                }
            }
            throw new Exception("Nie znaleziono miasta poczatkowego");
        }

        public int[] Najlepsza_trasa(int[][] tab_mrowek, int[][] tab_odleglosci)
        {
            double najlepsza_suma_odleglosci = odleglosc(tab_mrowek[0], tab_odleglosci);
            int index_najlepszej_sumy_odleglosci = 0;
            for (int i = 1; i < tab_mrowek.Length; i++)
            {
                double suma_odlegosci = odleglosc(tab_mrowek[i], tab_odleglosci);
                if (suma_odlegosci < najlepsza_suma_odleglosci)
                {
                    najlepsza_suma_odleglosci = suma_odlegosci;
                    index_najlepszej_sumy_odleglosci = i;
                }
            }
            int liczba_miast = tab_mrowek[0].Length;
            int[] tab_najlepszej_trasy = new int[liczba_miast];
            tab_mrowek[index_najlepszej_sumy_odleglosci].CopyTo(tab_najlepszej_trasy, 0);
            return tab_najlepszej_trasy;
        }

        public double odleglosc(int[] tab_trasy, int[][] tab_odleglosci)
        {

            double suma_odleglosc = 0.0;
            for (int i = 0; i <= tab_trasy.Length - 2; i++)
            {
                suma_odleglosc += dystans(tab_trasy[i], tab_trasy[i + 1], tab_odleglosci);
            }
            return suma_odleglosc;
        }


        public double dystans(int miastoX, int miastoY, int[][] tab_odleglosci)
        {
            return tab_odleglosci[miastoX][miastoY];
        }

        public double[][] rozloz_feromony(int liczba_miast)
        {
            double[][] tab_feromonow = new double[liczba_miast][];
            for (int i = 0; i < liczba_miast; i++)
            {
                tab_feromonow[i] = new double[liczba_miast];
            }
            for (int i = 0; i < tab_feromonow.Length; i++)
            {
                for (int j = 0; j < tab_feromonow[i].Length; j++)
                {
                    tab_feromonow[i][j] = 0.01;

                }
            }
            return tab_feromonow;
        }


        public void aktualizacja_mrowek(int[][] tab_mrowek, double[][] tab_feromonow, int[][] tab_odlegosci)
        {
            int liczba_miast = tab_feromonow.Length;
            for (int i = 0; i < tab_mrowek.Length; i++)
            {
                int poczatkowe_miasto = random.Next(0, liczba_miast);
                int[] tab_nowych_tras = buduj_trase(i, poczatkowe_miasto, tab_feromonow, tab_odlegosci);
                tab_mrowek[i] = tab_nowych_tras;
            }
        }

        public int[] buduj_trase(int i, int poczatkowe_miasto, double[][] tab_feromonow, int[][] tab_odleglosci)
        {
            int liczba_miast = tab_feromonow.Length;
            int[] tab_trasy = new int[liczba_miast];
            bool[] czy_odwiedzone = new bool[liczba_miast];
            tab_trasy[0] = poczatkowe_miasto;
            czy_odwiedzone[poczatkowe_miasto] = true;
            for (int j = 0; j < liczba_miast - 1; j++)
            {
                int miastoX = tab_trasy[j];
                int nastepne = nastepne_miasto(i, miastoX, czy_odwiedzone, tab_feromonow, tab_odleglosci);
                tab_trasy[j + 1] = nastepne;
                czy_odwiedzone[nastepne] = true;
            }
            return tab_trasy;
        }

        public int nastepne_miasto(int i, int miastoX, bool[] czy_odwiedzone, double[][] tab_feromonow, int[][] tab_odleglosci)
        {

            double[] prawdopodobienstwo = zmien_prawdopodobienstwo(i, miastoX, czy_odwiedzone, tab_feromonow, tab_odleglosci);

            double[] kumulacja = new double[prawdopodobienstwo.Length + 1];
            for (int j = 0; j <= prawdopodobienstwo.Length - 1; j++)
            {
                kumulacja[j + 1] = kumulacja[j] + prawdopodobienstwo[j];
            }

            double p = random.NextDouble();

            for (int j = 0; j <= kumulacja.Length - 2; j++)
            {
                if (p >= kumulacja[j] && p < kumulacja[j + 1])
                {
                    return j;
                }
            }
            throw new Exception("Bląd w metodzie nastepne_miasto");
        }

        public double[] zmien_prawdopodobienstwo(int i, int MiastoX, bool[] czy_odwiedzone, double[][] tab_feromonow, int[][] tab_odleglosci)
        {

            int liczba_miast = tab_feromonow.Length;
            double[] tab_prawdopodobienstwa = new double[liczba_miast];

            double suma = 0.0;

            for (int j = 0; j <= tab_prawdopodobienstwa.Length - 1; j++)
            {
                if (j == MiastoX)
                {
                    tab_prawdopodobienstwa[j] = 0.0;
                    // prawdopodobienstwo ze nastepne miasto bedzie tym, w ktorym sie znajduje wynosi 0
                }
                else if (czy_odwiedzone[j] == true)
                {
                    tab_prawdopodobienstwa[j] = 0.0;
                    // jesli odwiedzone tez 0
                }
                else
                {
                    tab_prawdopodobienstwa[j] = Math.Pow(tab_feromonow[MiastoX][j], alfa) * Math.Pow((1.0 / dystans(MiastoX, j, tab_odleglosci)), beta);
                    // potegowanie feromonow 

                    if (tab_prawdopodobienstwa[j] < 0.0001)
                    {
                        tab_prawdopodobienstwa[j] = 0.0001;
                    }
                    else if (tab_prawdopodobienstwa[j] > (double.MaxValue / (liczba_miast * 100)))
                    {
                        tab_prawdopodobienstwa[j] = double.MaxValue / (liczba_miast * 100);
                    }
                }
                suma += tab_prawdopodobienstwa[j];
            }

            double[] prawdopodbienstwo = new double[liczba_miast];
            for (int j = 0; j <= prawdopodbienstwo.Length - 1; j++)
            {
                prawdopodbienstwo[j] = tab_prawdopodobienstwa[j] / suma;
            }
            return prawdopodbienstwo;
        }



        public void aktualizacja_feromonow(double[][] tab_feromonow, int[][] tab_mrowek, int[][] tab_odleglosci)
        {
            for (int i = 0; i <= tab_feromonow.Length - 1; i++)
            {
                for (int j = i + 1; j <= tab_feromonow[i].Length - 1; j++)
                {
                    for (int k = 0; k <= tab_mrowek.Length - 1; k++)
                    {
                        double suma_odleglosci = odleglosc(tab_mrowek[k], tab_odleglosci);

                        double zmniejsz = (1.0 - a) * tab_feromonow[i][j];
                        double zwieksz = 0.0;
                        if (krawedz(i, j, tab_mrowek[k]) == true)
                        {
                            zwieksz = (b / suma_odleglosci);
                        }

                        tab_feromonow[i][j] = zmniejsz + zwieksz;

                        if (tab_feromonow[i][j] < 0.0001)
                        {
                            tab_feromonow[i][j] = 0.0001;
                        }
                        else if (tab_feromonow[i][j] > 100000.0)
                        {
                            tab_feromonow[i][j] = 100000.0;
                        }

                        tab_feromonow[j][i] = tab_feromonow[i][j];
                    }
                }
            }
        }

        public bool krawedz(int miastoX, int miastoY, int[] tab_trasy)
        {
            // sprawdza czy miasto x oraz y sasiaduja ze soba
            int ostatni_index = tab_trasy.Length - 1;
            int index = znajdz_poczatkowe_miasto(tab_trasy, miastoX);

            if (index == 0 && tab_trasy[1] == miastoY)
            {
                return true;
            }
            else if (index == 0 && tab_trasy[ostatni_index] == miastoY)
            {
                return true;
            }
            else if (index == 0)
            {
                return false;
            }
            else if (index == ostatni_index && tab_trasy[ostatni_index - 1] == miastoY)
            {
                return true;
            }
            else if (index == ostatni_index && tab_trasy[0] == miastoY)
            {
                return true;
            }
            else if (index == ostatni_index)
            {
                return false;
            }
            else if (tab_trasy[index - 1] == miastoY)
            {
                return true;
            }
            else if (tab_trasy[index + 1] == miastoY)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
