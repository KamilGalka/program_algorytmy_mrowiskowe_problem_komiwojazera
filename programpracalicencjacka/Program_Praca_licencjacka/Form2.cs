﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;


namespace Program_Praca_licencjacka
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }
        Metody metody = new Metody();
        Form_How_Many_Ants_And_Iteration form_how_many_ants_and_terations = new Form_How_Many_Ants_And_Iteration();
        Form_Help form_help = new Form_Help();
        Form_About form_about = new Form_About();
       
        private void button_count_Click(object sender, EventArgs e)
        {
          
               
                try
                {

                    int liczba_wierszy = 0;
                    int liczba_mrowek = int.Parse(Form_How_Many_Ants_And_Iteration.przekaz_liczbe_mrowek);
                    int liczba_iteracji = int.Parse(Form_How_Many_Ants_And_Iteration.przekaz_liczbe_iteracji);
                    progressBar1.Maximum = liczba_iteracji;

                    FileStream plik = new FileStream(Form1.przekaz_sciezke, FileMode.Open);
                    StreamReader odczyt = new StreamReader(plik);
                    string wiersz = odczyt.ReadLine();
                    while (odczyt.ReadLine() != null)
                    {
                        liczba_wierszy++;
                    }
                    int liczba_miast = liczba_wierszy + 1;
                    string[] odleglosci_jednego_wiersza = new string[liczba_wierszy + 1];
                    int[][] tab_odleglosci = new int[liczba_wierszy + 1][];
                    for (int i = 0; i <= liczba_wierszy; i++)
                    {
                        tab_odleglosci[i] = new int[liczba_wierszy + 1];
                    }
                    plik.Position = 0;

                    for (int i = 0; i <= liczba_wierszy; i++)
                    {
                        wiersz = odczyt.ReadLine();
                        for (int j = 0; j <= liczba_wierszy; j++)
                        {
                            odleglosci_jednego_wiersza = wiersz.Split(' ');
                            tab_odleglosci[i][j] = int.Parse(odleglosci_jednego_wiersza[j]);
                        }
                    }
                    plik.Close();

                    int[][] tab_mrowek = metody.utworz_mrowki(liczba_mrowek, liczba_miast);
                    int[] tab_najlepszej_trasy = metody.Najlepsza_trasa(tab_mrowek, tab_odleglosci);

                    double najlepsza_suma_odleglosci = metody.odleglosc(tab_najlepszej_trasy, tab_odleglosci);
                    double[][] tab_feromonow = metody.rozloz_feromony(liczba_miast);
                    int time = 0;
                    int ktora_iteracja = 0;
                    chart1.Series.Clear();
                    chart1.Series.Add("Best_lenght");
                    chart1.Series["Best_lenght"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
                    int start = Environment.TickCount & Int32.MaxValue;
                    while (time < liczba_iteracji)
                    {
                        
                        label3.Text = time.ToString();
                        label3.Update();
                        label6.Text = najlepsza_suma_odleglosci.ToString();
                        label6.Update();
                        metody.aktualizacja_mrowek(tab_mrowek, tab_feromonow, tab_odleglosci);
                        metody.aktualizacja_feromonow(tab_feromonow, tab_mrowek, tab_odleglosci);
                        int[] tab_obecnie_najlepszej_trasy = metody.Najlepsza_trasa(tab_mrowek, tab_odleglosci);
                        double obecnie_najlepsza_suma_odleglosci = metody.odleglosc(tab_obecnie_najlepszej_trasy, tab_odleglosci);
                        chart1.Series["Best_lenght"].Points.AddXY(time, obecnie_najlepsza_suma_odleglosci);
                        chart1.Update();
                        if (obecnie_najlepsza_suma_odleglosci < najlepsza_suma_odleglosci)
                        {
                            najlepsza_suma_odleglosci = obecnie_najlepsza_suma_odleglosci;
                            tab_najlepszej_trasy = tab_obecnie_najlepszej_trasy;
                            ktora_iteracja = time;
                        }
                        time += 1;
                        progressBar1.Value = time;

                    }
                    

                    textBox_wyswietlacz.Text += "The best sum of distances : " + najlepsza_suma_odleglosci.ToString() + "\r\n";
                    
                    

                    if (checkBox2.Checked)
                    {
                        textBox_wyswietlacz.Text += "\r\nVisited cities : [";
                        for (int i = 0; i < tab_najlepszej_trasy.Length; i++)
                        {
                            textBox_wyswietlacz.Text += " " + tab_najlepszej_trasy[i];
                        }
                        textBox_wyswietlacz.Text += " ] \r\n";
                    }
                    if (checkBox3.Checked)
                    {
                        textBox_wyswietlacz.Text += "\r\nThe best trial was found at " + ktora_iteracja + " iteration" + "\r\n";
                    }
                    textBox_wyswietlacz.Text += "----------------------------------------------------------------------------\r\n";




                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            
        }

        private void button_delete_results_Click(object sender, EventArgs e)
        {
            var wynik = MessageBox.Show("Are you sure you want to delete reults?", "Delete results", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (wynik == DialogResult.Yes)
            {
                textBox_wyswietlacz.Text = "";
                
            }
        }

        private void button_change_ants_iterations_Click(object sender, EventArgs e)
        {
            form_how_many_ants_and_terations.ShowDialog();
            
        }

        private void button_exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {
                this.Size = new System.Drawing.Size(1250, this.Size.Height);

            }
            else
            {
                this.Size = new System.Drawing.Size(700, this.Size.Height);
            }
        }

        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            form_help.ShowDialog();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            form_about.ShowDialog();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void saveResultsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string warunek = @".txt$";
            Regex r = new Regex(warunek, RegexOptions.IgnoreCase);
            OpenFileDialog dialog = new OpenFileDialog();

            if (textBox_wyswietlacz.Text!="")
            {
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    if (r.IsMatch(dialog.FileName))
                    {
                        System.IO.File.WriteAllText(dialog.FileName, textBox_wyswietlacz.Text);
                        MessageBox.Show("saving complete", "saving", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        MessageBox.Show("Choose file .txt", "Wrong file", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            else
            {
                MessageBox.Show("There is no results. \r\nClick on 'Count'","No results", MessageBoxButtons.OK,MessageBoxIcon.Error);
            }
            
        }

        private void zedGraphControl1_Load(object sender, EventArgs e)
        {
           
        }

        private void saveChartToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string warunek = @".jpg$";
            Regex r = new Regex(warunek, RegexOptions.IgnoreCase);
            OpenFileDialog dialog = new OpenFileDialog();
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                if (r.IsMatch(dialog.FileName))
                {
                    chart1.SaveImage(dialog.FileName, System.Windows.Forms.DataVisualization.Charting.ChartImageFormat.Png);
                    MessageBox.Show("saving complete", "saving", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("Choose file .jpg", "Wrong file", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

            
        }
    }
}
