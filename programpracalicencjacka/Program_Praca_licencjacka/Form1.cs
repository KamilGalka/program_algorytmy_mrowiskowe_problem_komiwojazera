﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace Program_Praca_licencjacka
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        
        Form2 form2 = new Form2();
        Form_How_Many_Ants_And_Iteration form_how_many_ants_and_iteration = new Form_How_Many_Ants_And_Iteration();
        Form_About form_about = new Form_About();
        Form_Help form_help = new Form_Help();
        bool wybrano_plik = false;
        public static bool przekaz_wybrano_plik;
        public static string przekaz_sciezke;

        private void runToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (wybrano_plik == true)
            {
                przekaz_wybrano_plik = wybrano_plik;
                form_how_many_ants_and_iteration.ShowDialog();

                form2.ShowDialog();
            }
            else
            {
                MessageBox.Show("Choose your file with distances", "There is no file", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            form_help.ShowDialog();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            form_about.ShowDialog();
        }

        private void openFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string warunek = @".txt$";
            Regex r = new Regex(warunek, RegexOptions.IgnoreCase);
            OpenFileDialog dialog = new OpenFileDialog();
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                if (r.IsMatch(dialog.FileName))
                {
                    wybrano_plik = true;
                    przekaz_sciezke = dialog.FileName;
                }
                else
                {
                    wybrano_plik = false;
                    MessageBox.Show("Choose file .txt", "Wrong file", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button_exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void yellowToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            
            
            
        }

        private void redToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.BackColor = System.Drawing.ColorTranslator.FromHtml("#ff1a1a");
            form2.BackColor = System.Drawing.ColorTranslator.FromHtml("#ff1a1a");
            form_about.BackColor = System.Drawing.ColorTranslator.FromHtml("#ff1a1a");
            form_help.BackColor = System.Drawing.ColorTranslator.FromHtml("#ff1a1a");
            form_how_many_ants_and_iteration.BackColor = System.Drawing.ColorTranslator.FromHtml("#ff1a1a");
        }

        private void whiteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.BackColor = Color.White;
            form2.BackColor = Color.White;
            form_about.BackColor = Color.White;
            form_help.BackColor = Color.White;
            form_how_many_ants_and_iteration.BackColor = Color.White;
        }

        private void blueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.BackColor = System.Drawing.ColorTranslator.FromHtml("#99B4D1");
            form2.BackColor = System.Drawing.ColorTranslator.FromHtml("#99B4D1");
            form_about.BackColor = System.Drawing.ColorTranslator.FromHtml("#99B4D1");
            form_help.BackColor = System.Drawing.ColorTranslator.FromHtml("#99B4D1");
            form_how_many_ants_and_iteration.BackColor = System.Drawing.ColorTranslator.FromHtml("#99B4D1");
        }

        private void greenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.BackColor = System.Drawing.ColorTranslator.FromHtml("#00e600");
            form2.BackColor = System.Drawing.ColorTranslator.FromHtml("#00e600");
            form_about.BackColor = System.Drawing.ColorTranslator.FromHtml("#00e600");
            form_help.BackColor = System.Drawing.ColorTranslator.FromHtml("#00e600");
            form_how_many_ants_and_iteration.BackColor = System.Drawing.ColorTranslator.FromHtml("#00e600");
        }

        private void activeCaptionToolStripMenuItem_Click(object sender, EventArgs e)
        {
           
        }
    }
}
