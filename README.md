Program prezentowany podczas obrony licencjackiej "Algorytmy mrowiskowe jako jedna z metod rozwiązywania problemu komiwojażera".
Dotyczy problemu komiwojażera.
Służy do wskazania najkrótszej trasy dla określonej liczby miast przy czym każde miasto odwiedzane jest tylko jeden raz, a następnie należy wrócić do miasta początkowego.

Podczas uruchomieniu aplikacji należy wgrać plik z odległościami zawarty w projekcie ( File > Open file), a następnie wybrać Start > Run i podać liczbę mrówek oraz iteracji.
